﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Task23BugLess.Helpers;
using Task23BugLess.Models;

namespace Task23.Controllers
{
    public class HomeController : Controller
    {
        private static List<Blog> _blogs;
        static HomeController()
        {
            _blogs = BlogModel.GetBlogs();
        }
        public ActionResult Index()
        {
            return View(_blogs);
        }

    }
}