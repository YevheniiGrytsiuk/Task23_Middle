﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Task23BugLess.Helpers;
using Task23BugLess.Models;

namespace Task23BugLess.Controllers
{
    public class QuestionnaireController : Controller
    {
        [ActionName("New")]
        [HttpGet]
        public ActionResult CreateNewPost()
        {
            return View();
        }
        [ActionName("New")]
        [HttpPost]
        public RedirectToRouteResult CreateNewPost(Blog blog)
        {
            if (ModelState.IsValid) 
            {
                AddBlog(blog);
                return RedirectToAction("Index", "Home");
            }
            return RedirectToAction("New", "Questionnaire");
        }
        private void AddBlog(Blog blog) 
        {
            var blogs = BlogModel.GetBlogs();

            if(blog.Id == 0) 
            {
                blogs.Add(new Blog() { Id = blogs.Last().Id, AuthorName = blog.AuthorName, Content = blog.Content, CreationTime = blog.CreationTime, Header = blog.Header });
            }
        }
    }
}