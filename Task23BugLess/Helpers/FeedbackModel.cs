﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Task23BugLess.Helpers;

namespace Task23BugLess.Models
{
    public static class FeedbackModel
    {
        private static List<Feedback> _feedbacks;
        static FeedbackModel() 
        {
            _feedbacks = new List<Feedback>()
            {
                new Feedback()
                {
                    BlogId = 0,
                    AuthorName = "megakiller_2009",
                    CreationTime = DateTime.Now,
                    Content = "Hello"
                },
                new Feedback()
                {
                    BlogId = 0,
                    AuthorName = "1223",
                    CreationTime = DateTime.Now,
                    Content = "Бобало геній"
                },
                new Feedback ()
                {
                    BlogId = 0,
                    AuthorName = "naruto@ss@in",
                    CreationTime = DateTime.Now,
                    Content = "люблю rap"
                },
                new Feedback() 
                {
                    BlogId = 0,
                    AuthorName = "test",
                    CreationTime = DateTime.Now,
                    Content = "завтра в школу"
                },
                new Feedback()
                {
                    BlogId = 0,
                    AuthorName = "megakiller_2009",
                    CreationTime = DateTime.Now,
                    Content = "Hello"
                },

                new Feedback() 
                {
                    BlogId = 1, 
                    AuthorName = "Nazar",
                    CreationTime = DateTime.Now,
                    Content = "Green bottle blue"
                },
                new Feedback ()
                {
                    BlogId = 1, 
                    AuthorName = "Math",
                    CreationTime = DateTime.Now,
                    Content = "люблю math"
                },
                new Feedback()
                {
                    BlogId = 1,
                    AuthorName = "testik111",
                    CreationTime = DateTime.Now, 
                    Content = "рівне макдональдс"
                },
                new Feedback()
                {
                    BlogId = 1,
                    AuthorName = "megakiller_2009",
                    CreationTime = DateTime.Now,
                    Content = "Hello"
                },

                new Feedback()
                {
                    BlogId = 2,
                    AuthorName = "1223",
                    CreationTime = DateTime.Now,
                    Content = "Бобало геній"
                },
                new Feedback ()
                {
                    BlogId = 2,
                    AuthorName = "naruto@ss@in",
                    CreationTime = DateTime.Now,
                    Content = "люблю rap"
                },

                new Feedback ()
                {
                    BlogId = 3,
                    AuthorName = "naruto@ss@in",
                    CreationTime = DateTime.Now,
                    Content = "люблю rap"
                },

                new Feedback()
                {
                    BlogId = 4,
                    AuthorName = "megakiller_2009",
                    CreationTime = DateTime.Now,
                    Content = "Hello"
                },
                new Feedback()
                {
                    BlogId = 4,
                    AuthorName = "1223",
                    CreationTime = DateTime.Now,
                    Content = "Бобало геній"
                }
            };
        }
        public static List<Feedback> GetFeedbacks() 
        {
            return _feedbacks;
        }
    }
}