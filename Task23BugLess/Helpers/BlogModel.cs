﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Task23BugLess.Helpers;

namespace Task23BugLess.Models
{
    public class BlogModel
    {
        private static List<Blog> _blogs;
        private static int AutoIncrement { get { return _autoIncrement++; } }
        private static int _autoIncrement;
        static BlogModel()
        {
            _blogs = new List<Blog>()
            {
                new Blog()
                {
                    Id = AutoIncrement,
                    Header =  "Getting started",
                    Content = "ASP.NET MVC gives you a powerful, patterns-based way to build dynamic websites that enables a clean separation of concerns and gives you full control over markup for enjoyable, agile development.",
                    AuthorName = "James",
                    CreationTime = DateTime.Now
                },
                 new Blog()
                {
                    Id = AutoIncrement,
                    Header =  "What's up? How you doing?",
                    Content = "Glaciers and ice sheets hold about 69 percent of the world's freshwater",
                    AuthorName = "Vito",
                    CreationTime = DateTime.Now
                },
                  new Blog()
                {
                    Id = AutoIncrement,
                    Header =  "Interesting fact",
                    Content = "The fastest gust of wind ever recorded on Earth was 253 miles per hour.",
                    AuthorName = "Alina",
                    CreationTime = DateTime.Now
                },
                   new Blog()
                {
                    Id = AutoIncrement,
                    Header =  "About hawaii",
                    Content = "The best place in the world to see rainbows is in Hawaii.",
                    AuthorName = "Jack",
                    CreationTime = DateTime.Now
                }, new Blog()
                {
                    Id = AutoIncrement,
                    Header =  "My name is not test",
                    Content = "WoW!!!!!",
                    AuthorName = "test",
                    CreationTime = DateTime.Now
                }
            };
        }

        public static List<Blog> GetBlogs() 
        {
            return _blogs;
        }
    }
    
}